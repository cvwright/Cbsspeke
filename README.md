# BS-SPEKE

This repo contains an early prototype implementation of the BS-SPEKE protocol
for augmented password authenticated key exchange (aPAKE).
The main repo is [bsspeke](https://gitlab.futo.org/cvwright/bsspeke); this version
has been adapted for easier use in Swift projects with Swift Package Manager.

BS-SPEKE is described by Steve Thomas in his blog post [SRP is Now Deprecated](https://tobtu.com/blog/2021/10/srp-is-now-deprecated/)
and in a [Github Gist](https://gist.github.com/Sc00bz/e99e48a6008eef10a59d5ec7b4d87af3).

The implementation contained here is built with Loup Vaillant's
[Monocypher](https://monocypher.org/) library.

It uses D.J. Bernstein's elliptic curve [Curve25519](https://cr.yp.to/ecdh.html).

### Disclaimer
This code has not undergone any sort of security audit or evaluation.
Please do not use it for anything serious at this time.
