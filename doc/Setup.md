# BS-SPEKE Setup Protocol

### Message 1 (client -> server)
This is the same as Message 1 in the login protocol.

The client sends the user's ID and a blind for the OPRF.

```
client_id: string
blind: base64-encoded curve point
```

### Message 2 (server -> client)
This is similar to one half of the server's Message 2 in
the login protocol.

The server multiplies its hashed salt by the client's blind
curve point to create the blind salt.

```
blind_salt: base64-encoded curve point
```

### Message 3 (client -> server)
Here the client finishes computation of the OPRF and returns
its public parameters to the server.

```
P: base64-encoded curve point
V: base64-encoded curve point
phf_params: {
    name: string
    blocks: integer
    iterations: integer
}
```

There's a valid question here:
"How does the server know that these values come from the real
client, and not from a MITM?"

The easy answer is that it's Trust On First Use (TOFU).

A true MITM in the network would not be able to defeat the TLS
session that protects this connection for an application like Matrix.

A malicious CDN could create arbitrary accounts, but they could
do that already with plaintext username and password.
Moreover, this reduces the server's vulnerability to the CDN;
now the adversary has to own the CDN at the moment when the password
is first set up.
Whereas before, owning the CDN at the time of any login was 
sufficient to learn the user's password.
And here, the CDN (and the server) still NEVER learn the 
password.
The most that the malicious CDN can do is to set their own fake
password for the victim user.

